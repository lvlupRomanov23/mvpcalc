package mvpCalc.model;

//import static model.Constant.TAG_CALC;
//import static model.Constant.TAG_CONVERTOR;

import static mvpCalc.model.Constant.TAG_CALC;
import static mvpCalc.model.Constant.TAG_CONVERTOR;

public class Factory {
    private static Factory instance;

    public static Factory getInstance() {
        if (instance != null)
            return instance;
        else
            return new Factory();
    }

    private Factory() {

    }

    public <TYPE, OPER, VAL1, VAL2> IFactory factory(IFactory.CallBack listener, Pair<TYPE, OPER, VAL1, VAL2> pair) {
        switch ((String) pair.getType()) {
            case TAG_CALC:
                return new Calc(listener, (Pair<String, String, Integer, Integer>) pair);
            case TAG_CONVERTOR:
                return new Convertor();
            default:
                throw new ArithmeticException();
        }
    }
}
