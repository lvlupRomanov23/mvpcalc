package mvpCalc.model;

public class Calc implements IFactory {
   private IFactory.CallBack listener;
   private  Pair<String,String,Integer,Integer> pair ;


    public Calc(IFactory.CallBack listener,Pair<String,String,Integer,Integer> pair) {
        this.listener = listener;
        this.pair = pair;
    }


    @Override
    public void calculation() {
        int result;
        switch (pair.getOper()){
            case "+":
                result = pair.getVal1() + pair.getVal2();
                listener.message("Result: " + result);
                break;
            case "-":
                result = pair.getVal1() - pair.getVal2();
                listener.message("Result: " + result);
                break;
            case "*":
                result = pair.getVal1() * pair.getVal2();
                listener.message("Result: " + result);
                break;
            case "/":
                if (pair.getVal2() == 0) {
                    listener.messageError("====  Низя так делать!  ====\n");
                    break;
                }
                result = pair.getVal1() / pair.getVal2();
                listener.message("Result: " + result);
                break;
        }
    }

}
