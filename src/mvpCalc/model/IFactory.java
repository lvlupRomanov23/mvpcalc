package mvpCalc.model;

public interface IFactory {
    interface CallBack{
        void message(String val);
        void messageError(String val);
    }

    void calculation();
}
