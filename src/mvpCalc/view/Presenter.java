package mvpCalc.view;


import mvpCalc.model.IFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Presenter implements IView.Presenter, IFactory.CallBack {
    private IView.View view;

    @Override
    public void init(IView.View view) {
        this.view = view;
    }

    @Override
    public String getEnterLine() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            return br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR";
    }

    @Override
    public String getOperation() {
        while (true){
            message("Enter operation + - * /");
            String operation = getEnterLine();
            if (operation.equals("-") || operation.equals("+") ||operation.equals("*") ||operation.equals("/") ){
                return operation;
            } else
                view.messageError("ERROR TRY AGAIN");
        }
    }

    @Override
    public Integer getFirstNumber() {
        while (true){
            message("Enter first number");
            String val =  getEnterLine();
            if (isInt(val)){
                return Integer.parseInt( val);
            }
        }
    }

    @Override
    public Integer getSecondNumber() {
        while (true){
            message("Enter second number");
            String val =  getEnterLine();
            if (isInt(val)){
                return Integer.parseInt( val);
            }
        }
    }

    boolean isInt(String val){
        try{
            Integer.parseInt(val);
            return true;
        } catch (Exception e){
            view.messageError("ERROR TRY AGAIN");
            return false;
        }
    }

    @Override
    public void message(String val) {
          view.message(val);
    }

    @Override
    public void messageError(String val) {
        view.messageError(val);
    }


}
