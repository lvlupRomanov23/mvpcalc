package mvpCalc.view;

import mvpCalc.model.Factory;
import mvpCalc.model.IFactory;
import mvpCalc.model.Pair;
import mvpCalc.view.IView;

import static mvpCalc.model.Constant.TAG_CALC;

public class View implements IView.View {
    private IView.Presenter presenter;
    private boolean flagExit;

    public View() {
        presenter = new Presenter();
        presenter.init(this);
    }

    @Override
    public void run() {
        while (!flagExit){
            message("Choose type: \n1 - Calc\n2 - Convertor\n0 - exit\n");
            switch (presenter.getEnterLine()) {
                case "1":
                    message("You choose Calc");
                    IFactory calc = Factory.getInstance().factory(presenter, new Pair<>(TAG_CALC, presenter.getOperation(), presenter.getFirstNumber(), presenter.getSecondNumber()));
                    calc.calculation();
                    break;
                case "2":
                    break;
                case "0":
                    flagExit = true;
                    break;
                default:
                    messageError("Error! Try again");


            }
        }
    }

    @Override
    public void message(String val) {
        System.out.println(val);
    }

    @Override
    public void messageError(String val) {
        System.err.println(val);
    }
}
